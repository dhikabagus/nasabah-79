const express = require('express');
const router = express.Router();
const transaction = require('../services/transaction');

/* GET All Transaction. */
router.get('/', async function (req, res, next) {
    try {
        res.json(await transaction.getAll(req.query.page));
    } catch (err) {
        console.error(`Error while getting transactions `, err.message);
        next(err);
    }
});

/* GET Transaction Report. */
router.get('/report', async function (req, res, next) {
    try {
        res.json(await transaction.getReport(req.body));
    } catch (err) {
        console.error(`Error while getting transactions `, err.message);
        next(err);
    }
});

/* POST new Transaction */
router.post('/', async function (req, res, next) {
    try {
        res.json(await transaction.create(req.body));
    } catch (err) {
        console.error(`Error while creating Transaction`, err.message);
        next(err);
    }
});

module.exports = router;