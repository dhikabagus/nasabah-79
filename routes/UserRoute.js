const express = require('express');
const router = express.Router();
const user = require('../services/user');

/* GET All User. */
router.get('/', async function (req, res, next) {
    try {
        res.json(await user.getAll(req.query.page));
    } catch (err) {
        console.error(`Error while getting users `, err.message);
        next(err);
    }
});

router.get('/:id', async function (req, res, next) {
    try {
        res.json(await user.getOne(req.params.id));
    } catch (err) {
        console.error(`Error while getting users `, err.message);
        next(err);
    }
});

/* POST new User */
router.post('/', async function (req, res, next) {
    try {
        res.json(await user.create(req.body));
    } catch (err) {
        console.error(`Error while creating User`, err.message);
        next(err);
    }
});

/* PUT a User */
router.put('/:id', async function (req, res, next) {
    try {
        res.json(await user.update(req.params.id, req.body));
    } catch (err) {
        console.error(`Error while updating User`, err.message);
        next(err);
    }
});

/* DELETE a User */
router.delete('/:id', async function (req, res, next) {
    try {
        res.json(await user.remove(req.params.id));
    } catch (err) {
        console.error(`Error while updating User`, err.message);
        next(err);
    }

})

module.exports = router;