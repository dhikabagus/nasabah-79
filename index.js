const express = require("express");
const app = express();
const port = 3000;

const user = require("./routes/UserRoute");
const transaction = require("./routes/TransactionRoute");

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

app.use("/api/v1/users", user);
app.use("/api/v1/transactions", transaction);

/* Error handler middleware */
app.use((err, req, res, next) => {
  const statusCode = err.statusCode || 500;
  console.error(err.message, err.stack);
  res.status(statusCode).json({ message: err.message });
  return;
});
app.listen(port, () => {
  console.log(`Nasabah-79 App listening at http://localhost:${port}`);
});