const db = require('./db');
const helper = require('../helper');
const config = require('../config');
const user = require('../services/User');

async function getAll(page = 1) {
    const offset = helper.getOffset(page, config.listPerPage);

    let query = `SELECT account_id, transaction_date, description, debit_credit_status, amount
                FROM tb_r_transaction 
                LIMIT ${offset},${config.listPerPage}`

    const rows = await db.query(query);
    const data = helper.emptyOrRows(rows);
    const meta = { page };

    return {
        data,
        meta
    }
}

async function getReport(params) {
    let query = `SELECT t.transaction_date,
                  t.description,
                  (CASE WHEN t.debit_credit_status = 'C' THEN amount END) AS Credit,
                  (CASE WHEN t.debit_credit_status = 'D' THEN amount END) AS Debit,
                  t.amount
                  FROM tb_r_transaction t
                  WHERE t.account_id =  ${params.accountId}
                  AND t.transaction_date >= '${params.startDate}'
                  AND t.transaction_date <= '${params.endDate}'
                  GROUP BY t.transaction_date;`
    const rows = await db.query(query);
    return rows
}

async function create(newTransaction) {
    let query = `INSERT INTO tb_r_transaction (account_id, transaction_date, description, debit_credit_status, amount)
    VALUES (${newTransaction.accountId},'${newTransaction.transactionDate}','${newTransaction.description}','${newTransaction.debitCreditStatus}',${newTransaction.amount})`
  
    const insertResult = await db.query(query);

    let message = 'Error in creating transaction';

    let bonusPoint = 0;
    let amountCount = newTransaction.amount
    if (newTransaction.description.toLowerCase() == "beli pulsa") {
        amountCount -= 10000
        if(amountCount > 30000){
            amountCount -= 30000
            bonusPoint += 20 + amountCount/500
        }else{
            bonusPoint += amountCount/1000
        }
    } else if (newTransaction.description.toLowerCase() == "bayar listrik") {
        amountCount -= 50000
        if(amountCount > 50000){
            amountCount -= 50000
            bonusPoint += 25 + amountCount/1000
        }else{
            bonusPoint += amountCount/2000
        }
    }

    const userData = await user.getOne(newTransaction.accountId)
    const totalPoint = userData[0].total_point + bonusPoint

    const insertBonus = await db.query(`UPDATE tb_m_account a 
    SET a.total_point = ${totalPoint} 
    WHERE a.account_id = ${newTransaction.accountId}`)
    
    if (insertResult.affectedRows) {
        message = 'Transaction success';
    }

    if(insertBonus.affectedRows){
        message += ' and Bonus Point Recieved'
    }

    return { message };
}

module.exports = {
    getAll,
    getReport,
    create
}