const db = require('./db');
const helper = require('../helper');
const config = require('../config');

async function getAll(page = 1) {
    const offset = helper.getOffset(page, config.listPerPage);
    const rows = await db.query(
        `SELECT account_id, name, total_point
    FROM tb_m_account LIMIT ${offset},${config.listPerPage}`
    );
    const data = helper.emptyOrRows(rows);
    const meta = { page };

    return {
        data,
        meta
    }
}

async function getOne(userId) {
    const data = await db.query(
        `SELECT account_id, name, total_point
        FROM tb_m_account
        WHERE account_id = ${userId}`
    );
    return data
}

async function create(newUser) {
    const result = await db.query(
      `INSERT INTO tb_m_account (name)
      VALUES ('${newUser.name}')`
    );

    let message = 'Error in creating user';

    if (result.affectedRows) {
        message = 'User created successfully';
    }
    return { message };
}

async function update(id, updatedUser){
    const result = await db.query(
      `UPDATE tb_m_account
      SET name="${updatedUser.name}"
      WHERE account_id=${id}`
    );
  
    let message = 'Error in updating User';
  
    if (result.affectedRows) {
      message = 'User updated successfully';
    }
  
    return {message};
}

async function remove(id){
    const result = await db.query(
        `DELETE FROM tb_m_account WHERE account_id = ${id};`
    )

    let message = 'Error in updating User';

    if (result.affectedRows) {
        message = 'User removed successfully';
    }
    return {message};
}

module.exports = {
    getAll,
    getOne,
    create,
    update,
    remove
}